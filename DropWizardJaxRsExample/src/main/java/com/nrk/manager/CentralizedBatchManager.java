package com.nrk.manager;

import io.dropwizard.hibernate.UnitOfWork;

import java.util.Date;
import java.util.List;

import com.nrk.persistence.entity.BatchOrder;

public class CentralizedBatchManager {

    private final BatchOrderService batchOrderService;

    public CentralizedBatchManager() {
        batchOrderService = new BatchOrderService();
    }

    @UnitOfWork
    public BatchOrder createBatchOrder(final String params,
            final int noOfTransactions, final Date timestamp) {
        BatchOrder createBatchOrder = batchOrderService.createBatchOrder(params,  timestamp);
        return createBatchOrder;
    }

    public List<BatchOrder> listAll() {
        return batchOrderService.listAll();
    }

}
