package com.nrk.manager;

import io.dropwizard.hibernate.UnitOfWork;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nrk.persistence.dao.BatchOrderDao;
import com.nrk.persistence.entity.BatchOrder;
import com.nrk.persistence.util.HibernateUtils;

public class BatchOrderService {

    private static final Logger LOG = LoggerFactory.getLogger(BatchOrderService.class);

    private final BatchOrderDao batchOrderDao;

    public BatchOrderService() {
        this.batchOrderDao = new BatchOrderDao();
    }

    @UnitOfWork
    public BatchOrder createBatchOrder(final String params, final Date timestamp) {
        BatchOrder batchOrder = null;
        LOG.info("Something done useful:{} ", timestamp);
        final Transaction txn = HibernateUtils.beginTransaction();
        try {
            batchOrder = batchOrderDao.createBatchOrder(params, timestamp, 1);
            HibernateUtils.commitTransaction(txn);
        } catch (Exception e) {
            HibernateUtils.rollbackTransaction(txn);
            HibernateUtils.<RuntimeException> rethrow(e);
        }
        return batchOrder;
    }

    public List<BatchOrder> listAll() {
        final Transaction txn = HibernateUtils.beginTransaction();
        try {
            List<BatchOrder> listAll = batchOrderDao.listAll();
            HibernateUtils.commitTransaction(txn);
            return listAll;
        } catch (Exception e) {
            HibernateUtils.rollbackTransaction(txn);
            HibernateUtils.<RuntimeException> rethrow(e);
        }
        return Collections.emptyList();
    }
}
