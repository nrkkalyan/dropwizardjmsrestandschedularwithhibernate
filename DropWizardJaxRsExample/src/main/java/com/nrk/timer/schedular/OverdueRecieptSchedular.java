package com.nrk.timer.schedular;

import io.dropwizard.lifecycle.Managed;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nrk.persistence.dao.BatchOrderDao;
import com.nrk.persistence.util.HibernateUtils;

public class OverdueRecieptSchedular implements Managed {
    private static final Logger LOG = LoggerFactory.getLogger(OverdueRecieptSchedular.class);

    private static OverdueRecieptSchedular overdueRecieptSchedular;
    private final ScheduledExecutorService scheduler;
    private final BatchOrderDao batchOrderDao;

    private OverdueRecieptSchedular() {
        this.batchOrderDao = new BatchOrderDao();
        this.scheduler = Executors.newScheduledThreadPool(1);
    }

    // Singleton
    public static OverdueRecieptSchedular getInstance() {
        if (overdueRecieptSchedular == null) {
            overdueRecieptSchedular = new OverdueRecieptSchedular();
        }
        return overdueRecieptSchedular;
    }

    void checkOverdueBatchReceipts(final Date time) {
        LOG.info("OverdueReceiptHandler Clock {}",
                DateFormatUtils.ISO_DATETIME_TIME_ZONE_FORMAT.format(time));
        final List<Long> batchOrderIds = batchOrderDao.findBatchesWithOverdueReceipts(time);
        LOG.info("Found {} batches with overdue receipts. BatchOrderIds: {}.", batchOrderIds.size(),
                batchOrderIds);
        if (CollectionUtils.isNotEmpty(batchOrderIds)) {
            LOG.info("Marking unreceipted batches as alarmed.");
            batchOrderDao.markBatchesAsAlarmed(batchOrderIds, time);
            LOG.info("Sending email for unreceipted batches.");
            String msg = "The following batch order ID have overdue receipt.";
            for (Long batchOrderId : batchOrderIds) {
                LOG.info("\nbatchOrderId{} \nstatus:{}, \nmessage:{}", batchOrderId, "OVERDUE_RECEIPT", msg);
            }
        }
        LOG.info("Done processing unreceipted batches.");
        LOG.info("checkBatches executed successful. Total time taken: {} MilliSeconds",
                System.currentTimeMillis() - time.getTime());
    }

    @Override
    public void start() throws Exception {
        final Runnable beeper = new Runnable() {
            public void run() {
                LOG.info("Attempting to find overtdue receptis .....");
                Transaction txn = HibernateUtils.beginTransaction();
                try {
                    checkOverdueBatchReceipts(new Date());
                    HibernateUtils.commitTransaction(txn);
                } catch (Exception e) {
                    HibernateUtils.rollbackTransaction(txn);
                    LOG.error("Ignored: Some exception occured. Next Retry in {} minutes", 5, e);
                }
            }
        };
        scheduler.scheduleAtFixedRate(beeper, 1, 1, TimeUnit.MINUTES);

    }

    @Override
    public void stop() throws Exception {
        this.scheduler.shutdown();
    }

}
