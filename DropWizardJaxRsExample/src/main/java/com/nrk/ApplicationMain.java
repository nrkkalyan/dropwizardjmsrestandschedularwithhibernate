package com.nrk;

import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.hibernate.SessionFactory;

import com.nrk.jms.BatchStatusMessageJmsListner;
import com.nrk.persistence.entity.BatchOrder;
import com.nrk.persistence.entity.BatchOrderEvent;
import com.nrk.persistence.util.HibernateUtils;
import com.nrk.resource.PingResource;
import com.nrk.timer.schedular.OverdueRecieptSchedular;

public class ApplicationMain extends Application<ApplicationConfiguration> {

    public static void main(String[] args) throws Exception {
        Configuration configuration = new PropertiesConfiguration("application.properties");
        ApplicationConfiguration.setConfig(configuration);

        new ApplicationMain().run(new String[] { "server" });
    }

    private final HibernateBundle<ApplicationConfiguration> hibernateBundle =
            new HibernateBundle<ApplicationConfiguration>(
                    BatchOrder.class,
                    BatchOrderEvent.class
            ) {
                @Override
                public DataSourceFactory getDataSourceFactory(ApplicationConfiguration configuration) {
                    return configuration.getDataSourceFactory();
                }
            };

    @Override
    public void initialize(Bootstrap<ApplicationConfiguration> bootstrap) {
        bootstrap.addBundle(hibernateBundle);
        bootstrap.addBundle(new AssetsBundle());
        bootstrap.addBundle(new MigrationsBundle<ApplicationConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(ApplicationConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });

        
       
    }

    @Override
    public void run(ApplicationConfiguration configuration, Environment environment) throws Exception {
        
        final SessionFactory sessionFactory = hibernateBundle.getSessionFactory();
        HibernateUtils.setSessionFactory(sessionFactory);
        registerRestResources(environment);
        registerSingletons(environment);

    }

    private void registerRestResources(Environment environment) {
        environment.jersey().register(new PingResource());
    }

    private void registerSingletons(Environment environment) {
        environment.lifecycle().manage( OverdueRecieptSchedular.getInstance());
        environment.lifecycle().manage( BatchStatusMessageJmsListner.getInstance());
    }

    @Override
    public String getName() {
        return "DropWizardJaxRsExample";
    }

}
