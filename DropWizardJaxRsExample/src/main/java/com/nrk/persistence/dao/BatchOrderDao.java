package com.nrk.persistence.dao;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nrk.persistence.dao.common.BaseEntityDao;
import com.nrk.persistence.entity.BatchOrder;
import com.nrk.persistence.entity.BatchOrderEvent;

public class BatchOrderDao extends BaseEntityDao<BatchOrder> {
    private static final Logger LOG = LoggerFactory.getLogger(BatchOrderDao.class);
    private static AtomicLong jobqId = new AtomicLong();

    
    public BatchOrderDao() {
        super();
    }

    public BatchOrder createBatchOrder(final String params,  
             final Date timestamp,
            final int receiptOverdueInHours) {
        final Date receiptDueDate =
                receiptOverdueInHours == 0 ? null : DateUtils.addHours(timestamp, receiptOverdueInHours);

        final BatchOrder batchOrder = new BatchOrder();
        batchOrder.setJobqId(System.nanoTime() + BatchOrderDao.jobqId.incrementAndGet());
        batchOrder.setBatchOrderParameters(params);
        batchOrder.setTimestamp(new Timestamp(timestamp.getTime()));
        batchOrder.setDueDate(receiptDueDate);
        batchOrder.setNumberOfTx(1);
        batchOrder.setStatus("PROCESSING");
        final BatchOrderEvent batchEvent = new BatchOrderEvent();
        batchEvent.setState("SCHEDULED");
        batchEvent.setTimestamp(timestamp);
        batchEvent.setMessage("BatchOrder message created.");
        batchOrder.addEvent(batchEvent);
        super.persist(batchOrder);
        return batchOrder;

    }


    public BatchOrder addBatchOrderEvent(final long batchOrderId, final String state,
            final String message, final Date date) {
        final BatchOrder batchOrder = get(batchOrderId);
        if (batchOrder == null) {
            throw new NullPointerException("Failed to find batch order ID " + batchOrderId
                    + " in database.");
        }

        final BatchOrderEvent event = new BatchOrderEvent();
        event.setState(state);
        event.setTimestamp(date);
        event.setMessage(message);
        batchOrder.addEvent(event);
        LOG.debug("Updating batch order id:{}", batchOrder.getId());

        batchOrder.setStatus(state);
        super.persist(batchOrder);
        LOG.info("Batch order updated: {}", batchOrder);
        return batchOrder;
    }

    @SuppressWarnings("unchecked")
    public List<Long> findBatchesWithOverdueReceipts(final Date date) {
        final DetachedCriteria completedOrFailedBatchIds = DetachedCriteria.forClass(BatchOrder.class);
        completedOrFailedBatchIds.add(Restrictions.gt("iTimestamp", twoWeeksAgo(date)));
        final DetachedCriteria events = completedOrFailedBatchIds.createCriteria("iEvents");
        events.add(Restrictions.or(Restrictions.eq("state", "COMPLETED"),
                Restrictions.eq("state", "FAILED")));
        completedOrFailedBatchIds.setProjection(Projections.distinct(Projections.property("iId")));

        final Criteria criteria = getCriteria();
        criteria.setProjection(Projections.distinct(Projections.property("iId")));
        criteria.add(Restrictions.isNull("iAlarmed"));
        // criteria.add(Restrictions.lt("iDueDate", date));
        // criteria.add(Restrictions.gt("iTimestamp", twoWeeksAgo(date)));
        // criteria.add(Subqueries.propertyNotIn("iId",
        // completedOrFailedBatchIds));
        // criteria.setReadOnly(true);
        return criteria.list();
    }

    private Criteria getCriteria() {
        return criteria();
    }

    private Date twoWeeksAgo(final Date date) {
        return DateUtils.addDays(date, -14);
    }

    @SuppressWarnings("unchecked")
    public void markBatchesAsAlarmed(final List<Long> batchOrderIds, final Date date) {
        final Criteria criteria = criteria()//
                .add(Restrictions.in("iId", batchOrderIds));
        // criteria.setReadOnly(true);
        final List<BatchOrder> batchOrders = criteria.list();
        for (final BatchOrder bo : batchOrders) {
            bo.setAlarmed(date);
        }
    }

    public List<BatchOrder> findBatchOrders(final Long batchOrderId, final Long jobId,
            final Date fromDate, final Date toDate, final int page, final int maxResult) {
        final Criteria criteria = createSearchCriteria(batchOrderId, jobId, fromDate, toDate);
        criteria.addOrder(Order.desc("iId"));
        if (page > 0 && maxResult > 0) {
            criteria.setFirstResult((page - 1) * maxResult).setMaxResults(maxResult);
        }
        return list(criteria);
    }

    private Criteria createSearchCriteria(final Long batchOrderId, final Long jobqId, Date fromDate,
            Date toDate) {
        final Criteria criteria = criteria();//
        if (batchOrderId != null) {
            criteria.add(Restrictions.eq("iId", batchOrderId));
        }

        if (jobqId != null) {
            criteria.add(Restrictions.eq("jobqId", jobqId));
        }

        if (batchOrderId == null && fromDate == null && toDate == null) {
            toDate = fromDate = new Date();
        }

        if (fromDate != null) {
            final Calendar c = Calendar.getInstance();
            c.setLenient(false);
            c.setTime(fromDate);
            // Night 00.00.00
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            fromDate = c.getTime();
        }

        if (toDate != null) {
            final Calendar c = Calendar.getInstance();
            c.setLenient(false);
            c.setTime(toDate);
            // Night 23.59.59.999
            c.set(Calendar.HOUR_OF_DAY, 23);
            c.set(Calendar.MINUTE, 59);
            c.set(Calendar.SECOND, 59);
            c.set(Calendar.MILLISECOND, 999);
            toDate = c.getTime();
        }

        if (fromDate != null && toDate != null) {
            criteria.add(Restrictions.between("iTimestamp", fromDate, toDate));
        } else if (fromDate == null && toDate != null) {
            final Calendar c = Calendar.getInstance();
            c.setLenient(false);
            c.setTime(toDate);
            // Night 00.00.00
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            criteria.add(Restrictions.between("iTimestamp", c.getTime(), toDate));
        } else if (fromDate != null && toDate == null) {
            final Calendar c = Calendar.getInstance();
            c.setLenient(false);
            c.setTime(fromDate);
            // Night 23.59.59
            c.set(Calendar.HOUR_OF_DAY, 23);
            c.set(Calendar.MINUTE, 59);
            c.set(Calendar.SECOND, 59);
            c.set(Calendar.MILLISECOND, 999);
            criteria.add(Restrictions.between("iTimestamp", fromDate, c.getTime()));
        }
        return criteria;
    }

    public long getCount(final Long batchOrderId, final Long jobqId, final Date fromDate,
            final Date toDate) {

        final Criteria criteria =
                createSearchCriteria(batchOrderId, jobqId, fromDate, toDate).setProjection(
                        Projections.rowCount());

        Long count = (Long) criteria.uniqueResult();
        if (count == null) {
            return 0;
        }
        return count;
    }

    public long getFailedBatchOrdersCount() {
        final Criteria criteria = criteria();//
        criteria.add(Restrictions.eq("status", "FAILED"));
        criteria.setProjection(Projections.rowCount());

        Long count = (Long) criteria.uniqueResult();
        if (count == null) {
            return 0;
        }
        return count;
    }

}
