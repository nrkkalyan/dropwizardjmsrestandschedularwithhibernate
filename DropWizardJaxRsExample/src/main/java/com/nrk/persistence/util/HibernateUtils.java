package com.nrk.persistence.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import static com.google.common.base.Preconditions.checkNotNull;

public final class HibernateUtils {

    private static SessionFactory sessionFactory;
    
    public static Transaction beginTransaction() {
        Session session = sessionFactory.getCurrentSession();
       return session.beginTransaction();
        
    }

    public static void rollbackTransaction(Transaction txn) {
        if (txn != null && txn.isActive()) {
            txn.rollback();
        }
    }

    public static void commitTransaction(Transaction txn) {
        if (txn != null && txn.isActive()) {
            txn.commit();
        }
    }
    
    @SuppressWarnings("unchecked")
    public static <E extends Exception> void rethrow(Exception e) throws E {
        throw (E) e;
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static void setSessionFactory(SessionFactory sf) {
        sessionFactory = checkNotNull(sf,"SessionFactory shall never be null.");
    }

}
