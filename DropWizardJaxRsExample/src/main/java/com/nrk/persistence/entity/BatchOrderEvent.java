package com.nrk.persistence.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@Table(name = "BATCH_ORDER_EVENT")
public class BatchOrderEvent extends BaseEntity {
    private static final long serialVersionUID = 1L;
    public static final int MAX_MESSAGE_SIZE = 1024;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BOE_ID", unique = true, nullable = false, updatable = false)
    private Long id;

    @ManyToOne()
    @JoinColumn(name = "BOE_BATCH_ORDER_ID", unique = false, nullable = false, updatable = false)
    private BatchOrder batchOrder;

    @Column(name = "BOE_TIMESTAMP", unique = false, nullable = false, updatable = false)
    private Date date = new Date();

    @Column(name = "BOE_STATE", unique = false, nullable = false, updatable = true)
    private String state;

    @Column(name = "BOE_MESSAGE", unique = false, nullable = true, updatable = true,
            length = MAX_MESSAGE_SIZE)
    private String message;

    @Override
    public Long getId() {
        return id;
    }
    
    public void setId(final Long iId) {
        this.id = iId;
    }


    void setBatchOrder(final BatchOrder batchOrder) {
        this.batchOrder = batchOrder;
    }

    public Date getTimestamp() {
        return date;
    }

    public void setTimestamp(final Date date) {
        this.date = date;
    }

    public String getState() {
        return state;
    }

    public String getBatchState() {
        return state;
    }

    public void setState(final String state) {
        this.state = state;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = StringUtils.substring(message, 0, MAX_MESSAGE_SIZE);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
