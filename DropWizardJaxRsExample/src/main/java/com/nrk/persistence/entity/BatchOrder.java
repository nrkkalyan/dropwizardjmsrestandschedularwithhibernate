package com.nrk.persistence.entity;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mahlin
 */
@Entity
@Table(name = "BATCH_ORDER")
@XmlRootElement
public class BatchOrder extends BaseEntity {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BO_ID", unique = true)
    private Long iId;

    @Column(name = "BO_TIMESTAMP", unique = false, nullable = false, updatable = true)
    private Timestamp iTimestamp;

    @Column(name = "BO_NUMBER_OF_TX", unique = false, nullable = false, updatable = true)
    private Integer iNumberOfTx;

    @Column(name = "BO_DUE_DATE", unique = false, nullable = true, updatable = true)
    private Date iDueDate;

    @Column(name = "BO_ALARMED", unique = false, nullable = true, updatable = true)
    private Date iAlarmed;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "batchOrder", fetch=FetchType.EAGER)
    private Set<BatchOrderEvent> iEvents = new HashSet<BatchOrderEvent>();

    @Column(name = "BO_PARAMETERS", updatable = false, nullable = true)
    private String batchOrderParameters;

    @Column(name = "BO_JOBQ_ID", unique = true, updatable = false, nullable = true)
    private Long jobqId;

    @Column(name = "BO_STATUS", updatable = true, nullable = true)
    private String status;

    @Override
    public Long getId() {
        return iId;
    }

    public void setId(final Long iId) {
        this.iId = iId;
    }

    public Timestamp getTimestamp() {
        return iTimestamp;
    }

    public void setTimestamp(final Timestamp iTimestamp) {
        this.iTimestamp = iTimestamp;
    }

    public Integer getNumberOfTx() {
        return iNumberOfTx;
    }

    public void setNumberOfTx(final Integer iNumberOfTx) {
        this.iNumberOfTx = iNumberOfTx;
    }

    public Date getDueDate() {
        return iDueDate;
    }

    public void setDueDate(final Date iDueDate) {
        this.iDueDate = iDueDate;
    }

    public Date getAlarmed() {
        return iAlarmed;
    }

    public void setAlarmed(final Date iAlarmed) {
        this.iAlarmed = iAlarmed;
    }

    public Set<BatchOrderEvent> getEvents() {
        return iEvents;
    }

    public void setEvents(final Set<BatchOrderEvent> iEvents) {
        this.iEvents = iEvents;
    }

    public String getBatchOrderParameters() {
        return batchOrderParameters;
    }

    public void setBatchOrderParameters(final String batchOrderParameters) {
        this.batchOrderParameters = batchOrderParameters;
    }

    public Long getJobqId() {
        return jobqId;
    }

    public void setJobqId(final Long jobqId) {
        this.jobqId = jobqId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    public void addEvent(final BatchOrderEvent batchEvent) {
        batchEvent.setBatchOrder(this);
        iEvents.add(batchEvent);
    }


}
