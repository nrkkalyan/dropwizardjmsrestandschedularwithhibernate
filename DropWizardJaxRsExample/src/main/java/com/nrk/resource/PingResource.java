package com.nrk.resource;

import io.dropwizard.hibernate.UnitOfWork;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nrk.manager.CentralizedBatchManager;
import com.nrk.persistence.entity.BatchOrder;

@Path("ping")
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_JSON })
public class PingResource {
    private static final Logger log = LoggerFactory.getLogger(PingResource.class);

    private final CentralizedBatchManager batchManager;

    public PingResource() {
        batchManager = new CentralizedBatchManager();
    }

    @POST
    @UnitOfWork
    public String createBatchOrder(
            @QueryParam("params") final String params,
            @QueryParam("noOfTransactions") final int noOfTransactions
            ) {
        log.info("Creating batchOrders params:{}, noOfTxn:{}", params, noOfTransactions);
        BatchOrder bo= batchManager.createBatchOrder(params, noOfTransactions, new
                Date());
        return new Date() +"" + bo;
    }
    
    @GET
    @UnitOfWork
    public List<BatchOrder> getAllBatchOrders() {
        List<BatchOrder> listAll = new ArrayList<BatchOrder>();
        for (BatchOrder batchOrder : batchManager.listAll()) {
            listAll.add(batchOrder);
        } 
        
        return listAll;
    }
    
    

}
