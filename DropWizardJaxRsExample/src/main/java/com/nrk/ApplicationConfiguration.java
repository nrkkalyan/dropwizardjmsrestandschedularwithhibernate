package com.nrk;

import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.jetty.HttpConnectorFactory;
import io.dropwizard.server.DefaultServerFactory;

import java.util.HashMap;
import java.util.Map.Entry;

import org.hibernate.cfg.AvailableSettings;

public class ApplicationConfiguration extends Configuration{
    
    private static org.apache.commons.configuration.Configuration configuration;
    
    
    

    public ApplicationConfiguration() {
        super();
        
        ((HttpConnectorFactory) ((DefaultServerFactory) getServerFactory()).getApplicationConnectors().get(0)).setPort(8080);
        // this is for admin port
        ((HttpConnectorFactory) ((DefaultServerFactory) getServerFactory()).getAdminConnectors().get(0)).setPort(8090);  
    
    }

    

    public DataSourceFactory getDataSourceFactory() {

        final DataSourceFactory database = new DataSourceFactory();
        final org.hibernate.cfg.Configuration hibernateConfiguration = 
                            new org.hibernate.cfg.Configuration().configure("hibernate.cfg.xml");
        HashMap<String, String> properties = new HashMap<String, String>();
        for (Entry<?, ?> property : hibernateConfiguration.getProperties().entrySet()) {
            properties.put(property.getKey().toString(), property.getValue().toString());
        }
        properties.put(AvailableSettings.SHOW_SQL, "true");
        properties.put(AvailableSettings.HBM2DDL_AUTO, "update");
        
        database.setProperties(properties);

        database.setUrl("jdbc:mysql://localhost:3306/test");
        database.setUser("admin");
        database.setPassword("admin");
        database.setDriverClass("com.mysql.jdbc.Driver");
        
//        database.setUrl("jdbc:h2:./target/example");
//        database.setUser("sa");
//        database.setPassword("sa");
//        database.setDriverClass("org.h2.Driver");
        
        return database;
    }



    public static org.apache.commons.configuration.Configuration getConfig() {
        return configuration;
    }

    public static void setConfig(org.apache.commons.configuration.Configuration configuration) {
        ApplicationConfiguration.configuration = configuration;
    }

    
}
