package com.nrk.jms;

import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.lifecycle.Managed;

import javax.jms.Message;
import javax.jms.TextMessage;

import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nrk.ApplicationConfiguration;
import com.nrk.persistence.dao.BatchOrderDao;
import com.nrk.persistence.util.HibernateUtils;

public class BatchStatusMessageJmsListner extends AbstractJmsQueuePoller implements Managed {
    private static final Logger LOG = LoggerFactory.getLogger(BatchStatusMessageJmsListner.class);

    private static BatchStatusMessageJmsListner batchStatusMessageJmsListner;
    private final BatchOrderDao bathOrderDao;

    private BatchStatusMessageJmsListner() {
        this.bathOrderDao = new BatchOrderDao();
    }

    public static BatchStatusMessageJmsListner getInstance() {
        if (batchStatusMessageJmsListner == null) {
            batchStatusMessageJmsListner = new BatchStatusMessageJmsListner();
        }
        return batchStatusMessageJmsListner;
    }

    public void start() {
        LOG.info("Initializing JMS Connection...");
        try {
            init();
        } catch (Exception e2) {
            throw new RuntimeException(e2);
        }
        LOG.info("Successfully JMS Connection established");

    }

    @Override
    protected JmsDestination getInJmsDestination() {
        return new JmsDestination(ApplicationConfiguration.getConfig().getString("openmq.host"),
                ApplicationConfiguration.getConfig().getString("openmq.BatchStatusMessage.queue"),
                ApplicationConfiguration.getConfig().getString("openmq.BatchStatusMessage.queue"));
    }

    @Override
    protected JmsDestination getErrorQueueJmsDestination() {
        return new JmsDestination(ApplicationConfiguration.getConfig().getString("openmq.host"),
                ApplicationConfiguration.getConfig().getString("openmq.BatchStatusMessage.error.queue"),
                ApplicationConfiguration.getConfig().getString("openmq.BatchStatusMessage.error.queue"));
    }

    @Override
    protected int getPollIntervalMinutes() {
        return 10;
    }

    @Override
    @UnitOfWork
    protected void afterReadingMessage(Message message) throws Exception {
        Transaction txn = HibernateUtils.beginTransaction();
        try {
            LOG.debug("Message received.");
            if (!(message instanceof TextMessage)) {
                throw new IllegalArgumentException("Invalid message type");
            }
            final TextMessage textMessage = (TextMessage) message;
            LOG.debug("Converting message to BatchStatusMessage.\n{}", textMessage.getText());
            LOG.info("batchesWithOverdueReceipts \n:{}", bathOrderDao.listAll());
            HibernateUtils.commitTransaction(txn);
        } catch (Exception e) {
            HibernateUtils.rollbackTransaction(txn);
            HibernateUtils.<RuntimeException> rethrow(e);
        }
    }

    @Override
    protected void beforeReadingMessage() throws Exception {
        LOG.debug("About to read jms message.");
    }

    @Override
    public void stop() throws Exception {
        shutdown();
    }

}
