package com.nrk.jms;

import javax.jms.JMSException;
import javax.jms.Session;

public interface JmsCallback<T> {
    T doInJmsSession(Session s) throws JMSException;

}