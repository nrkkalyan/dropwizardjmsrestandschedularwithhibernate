package com.nrk.jms;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.messaging.ConnectionConfiguration;
import com.sun.messaging.ConnectionFactory;
import com.sun.messaging.jms.Connection;
import com.sun.messaging.jms.notification.Event;
import com.sun.messaging.jms.notification.EventListener;

/*
 * This class is a legacy class (built on top of JmsQueuePoller) acting as a
 * MessageListner for a configured queue. In order to to keep the existing
 * behavior I have modified this class little bit so that we can use this class
 * in anywhere
 * 
 * @author : rnimmagadda
 */
public abstract class AbstractJmsQueuePoller {
    private static final Logger LOG = LoggerFactory.getLogger(AbstractJmsQueuePoller.class);

    private static final int MAX_CONSECUTIVE_ERRORS = 5;
    private static final AtomicBoolean isAlive = new AtomicBoolean(false);

    private final AtomicLong consecutiveErrors = new AtomicLong(0L);

    protected abstract JmsDestination getInJmsDestination();

    protected abstract JmsDestination getErrorQueueJmsDestination();

    protected abstract int getPollIntervalMinutes();

    protected abstract void afterReadingMessage(Message message) throws Exception;

    protected abstract void beforeReadingMessage() throws Exception;
    
    private final BeeperControl jmsConnectionMonitor = new BeeperControl();

    public AbstractJmsQueuePoller() {
        jmsConnectionMonitor.monitorJmsConnection();
    }

    protected void init() throws Exception {
        try {
            final ConnectionFactory cf = new ConnectionFactory();
            cf.setProperty(ConnectionConfiguration.imqAddressList, getInJmsDestination().getHost());

            final Connection connection = (Connection) cf.createConnection();

            // Set the Exception listner
            connection.setExceptionListener(new ExceptionListener() {
                @Override
                public void onException(JMSException ignore) {
                    LOG.error("Exception occured in JMS Broker. Message Details:{}", ignore);
                    isAlive.set(false);
                }
            });

            // Set the Event listner
            connection.setEventListener(new EventListener() {
                @Override
                public void onEvent(Event connEvent) {
                    String eventCode = connEvent.getEventCode();
                    String eventMessage = connEvent.getEventMessage();

                    if (StringUtils.isNotBlank(eventCode)) {
                        if (eventCode.startsWith("E2") || eventCode.equals("E401")) {
                            LOG.error("JMS broker is DISCONNECTED.");
                            isAlive.set(false);

                        } else if (eventCode.equals("E301")) {
                            LOG.info("CONNECTED successfully to JMS broker.");
                            isAlive.set(true);
                        }
                    }

                    LOG.info("Received JMS notification event. EventCode={}, EventMessage={}. JMS Connection={}",
                            eventCode, eventMessage, isAlive() ? "Connected" : "Disconnected");

                }
            });

            connection.start();
            final Session session = connection.createSession(true, Session.SESSION_TRANSACTED);
            final String queueName = getInJmsDestination().getQueueName();
            final Queue queue = session.createQueue(queueName);
            final MessageConsumer consumer = session.createConsumer(queue);
            consumer.setMessageListener(new MessageListener() {
                @Override
                public void onMessage(Message message) {
                    LOG.info("Processing JMS message {}, queuename:{}", message, queueName);
                    onMessageArrive(session, message);
                }
            });
            isAlive.set(true);
        } finally {
//            final Thread jmsMonitorThread = new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    while (true) {
//                        try {
//                            if (!isAlive()) {
//                                LOG.info("Attempting to reconnect to JMS Broker......");
//                                init();
//                                LOG.info("Successfully connection to JMS Broker");
//                            }
//                        } catch (Exception e) {
//                            LOG.error("Could not re-connect to JMS Broker. Next Retry in {} seconds. Reason:{}",
//                                    30, e.toString());
//                            try {
//                                Thread.sleep(30000);
//                            } catch (InterruptedException e1) {
//                                e1.printStackTrace();
//                            }
//                        }
//                    }
//                }
//            });
//            jmsMonitorThread.setDaemon(true);
//            jmsMonitorThread.start();
        }
    }

    private void onMessageArrive(final Session session, final Message message) {
        try {
            beforeReadingMessage();
            afterReadingMessage(message);
            consecutiveErrors.set(0L);
        } catch (Exception e) {
            sendToErrorQueue(session, message, e);
            LOG.warn("Ignored: Exception while processing message: {}", message, e);
            LOG.warn("Consecutive error count = {}, max = {}.", consecutiveErrors.incrementAndGet(),
                    MAX_CONSECUTIVE_ERRORS);
            if (consecutiveErrors.get() % MAX_CONSECUTIVE_ERRORS == 0) {
                notifyMaxConsecutiveErrorsReached(message, e);
            }
        } finally {
            try {
                session.commit();
            } catch (JMSException ignore) {
                LOG.warn("Exception occured while commiting session. Message Details:{}", message, ignore);
                rollbackLogException(session);
            }
        }
    }

    public static boolean isAlive() {
        return isAlive.get();
    }

    public static void shutdown() {
        isAlive.set(false);
    }

    private void notifyMaxConsecutiveErrorsReached(Message message, Exception e) {
        LOG.info("Attempting to send mail.");
        final StringBuilder sb = new StringBuilder();
        sb.append("Exception occured for the given JMS message. Please investigate.");
        sb.append(message);
        sb.append(" Exception is: ");
        sb.append(e);
        LOG.warn("Max consecutive errors reached: {},{}", consecutiveErrors.get(), sb.toString());
    }

    private void sendToErrorQueue(final Session session, final Message message, Exception e) {
        LOG.info("Attempting to send mail before placing message on error queue.");
        try {
            final StringBuilder sb = new StringBuilder();
            sb.append("LogTraceId:[");
            sb.append(']');
            sb.append("Could not process a jms message from queue:");
            sb.append(getInJmsDestination());
            sb.append(" Exception is: ");
            sb.append(e);
            sb.append("\nThe message will be moved to the error queue.\n");
            sb.append("JMS Message:").append(message);
            sb.append("\n\n");
            sb.append("Stack trace is:\n");
            sb.append(getStackTrace(e));
            final String emailMessage = sb.toString();
            LOG.warn("Message placed on error queue.:{}", emailMessage);
        } finally {
            sendToErrorQueue(session, message);
        }

    }

    private static String getStackTrace(Throwable aThrowable) {
        final Writer result = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(result);
        aThrowable.printStackTrace(printWriter);
        return result.toString();
    }

    private void sendToErrorQueue(Session session, Message message) {
        final String errorQueue = getErrorQueueJmsDestination().getQueueName();
        MessageProducer producer = null;
        try {
            Queue queue = session.createQueue(errorQueue);
            producer = session.createProducer(queue);
            LOG.info("Sending JMS message to error queue {}. Message: {}", errorQueue, message);
            producer.send(message);
            LOG.info("JMS message id: '{}' successfully sent to error queue {}.", message.getJMSMessageID(), errorQueue);
        } catch (JMSException e) {
            LOG.warn("Exception while sending message to error queue.", e);
        } finally {
            if (producer != null) {
                try {
                    producer.close();
                    LOG.info("JMS producer closed.");
                } catch (JMSException e) {
                    LOG.warn("Exception while closing producer.", e);
                }
            }
        }
    }

    private static void rollbackLogException(final Session session) {
        try {
            LOG.info("Rolling back session.");
            session.rollback();
        } catch (JMSException e) {
            LOG.warn("Exception while rolling back session.", e);
        }
    }

    public static void testConnection(final String jmsHost) throws JMSException {
        runInTransactedSession(jmsHost, new JmsCallback<Class<Void>>() {
            public Class<Void> doInJmsSession(Session s) throws JMSException {
                LOG.debug("Successfully connected to {}.", jmsHost);
                return Void.TYPE;
            }
        });
    }

    public static <T> T runInTransactedSession(String jmsUrl, JmsCallback<T> jmsCallback) throws JMSException {
        LOG.debug("Creating connection to {}.", jmsUrl);
        final javax.jms.Connection c = createConnection(jmsUrl);
        try {
            LOG.debug("Starting connection.");
            c.start();
            try {
                return createTransactedSessionAndRun(c, jmsCallback);
            } finally {
                LOG.debug("Stopping connection.");
                c.stop();
            }
        } finally {
            c.close();
            LOG.debug("Connection closed to {}.", jmsUrl);
        }
    }

    private static javax.jms.Connection createConnection(String jmsUrl) throws JMSException {
        final ConnectionFactory f = new ConnectionFactory();
        f.setProperty(ConnectionConfiguration.imqAddressList, jmsUrl);
        javax.jms.Connection createConnection = f.createConnection();
        return createConnection;
    }

    private static <T> T createTransactedSessionAndRun(javax.jms.Connection c, JmsCallback<T> jmsCallback) throws JMSException {
        final Session session = c.createSession(true, Session.SESSION_TRANSACTED);
        try {
            return jmsCallback.doInJmsSession(session);
        } catch (RuntimeException | JMSException e) {
            rollbackLogException(session);
            throw e;
        } finally {
            session.close();
            LOG.debug("Session closed.");
        }
    }

    private final class BeeperControl {
        private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

        public void monitorJmsConnection() {
            final Runnable beeper = new Runnable() {
                public void run() {
                    if (!isAlive()) {
                        LOG.info("Attempting to reconnect to JMS Broker......");
                        try {
                            init();
                            LOG.info("Successfully connection to JMS Broker");
                        } catch (Exception e) {
                            LOG.error("Could not re-connect to JMS Broker. Next Retry in {} seconds. Reason:{}",
                                    30, e.toString());
                        }
                    }
                }
            };
            scheduler.scheduleAtFixedRate(beeper, 10, 30, TimeUnit.SECONDS);

        }
    }

}
