package com.nrk.jms;

import org.apache.commons.lang.builder.ToStringBuilder;

public class JmsDestination {
    private final String host;
    private final String queueName;
    private final String queuePropertyName;
    private final String topicName;

    public JmsDestination(String jmsHost, String queueName, String queuePropertyName) {
        this.host = jmsHost;
        this.queueName = queueName;
        this.queuePropertyName = queuePropertyName;
        this.topicName = null;
    }

    public JmsDestination(String host, String topicName) {
        super();
        this.host = host;
        this.queueName = null;
        this.queuePropertyName = null;
        this.topicName = topicName;
    }

    public boolean isQueue() {
        return topicName == null;
    }

    public String getHost() {
        return host;
    }

    public String getTopicName() {
        return topicName;
    }

    public String getQueueName() {
        return queueName;
    }

    public String getQueuePropertyName() {
        return queuePropertyName;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
